# -*- coding: utf-8 -*-

# Scrapy settings for vaccine project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'vaccine'

SPIDER_MODULES = ['vaccine.spiders']
NEWSPIDER_MODULE = 'vaccine.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'vaccine (+http://www.yourdomain.com)'

# Obey robots.txt rules
# ROBOTSTXT_OBEY = false

DEFAULT_REQUEST_HEADERS = {
    'Referer': 'http://www.nicpbp.org.cn/CL0233/',
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
    'Cookie':'locale=zh_TW; slocale=1; currency=HKD; m2=0; _ga=GA1.2.1189777043.1476255596; fbm_197994114318=base_domain=.pinkoi.com; b=20161013CZChW9CAd5; __zlcmid=d4fv1uGQIIi52i; csrf=gAJ9cQBVBGNzcmZxAVgUAAAAWU9Na0M3ZEhla1BiUTNpejJ4eE1xAnMuWMti2XNYw0T9aCANKRExEmm5sdaMN0Fd; __asc=c0bda17215ada5eb18946c7346f; __auc=1847e42c157b7b0bde4916b8a44; __utma=52696361.1189777043.1476255596.1489718657.1489722323.15; __utmb=52696361.2.10.1489722323; __utmc=52696361; __utmz=52696361.1476255596.1.1.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; fbsr_197994114318=oC8GWW95LmEGYTx-z0m1w4DsffZVV1cnf3yl1hFZ2UI.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUIzOUwzN1JYd1FyWU9vVFNPRDZjcmplTndWcnBhd1dyVDBHVlY3bjQwd2tMR1Z4QTk2bk1sQWFzei1CQU54TmEwRVRRQm9ENGt4R2Y2UmI2ck81OUk4U0p6Z2tKczdfdzlDMnFiVnN2M0RTVGYxSGg3OTI0dWJsUmRrRFRWa19uNHU4ZXoxQzVXTGY2VzY0VHUxY3pHS3dVMUI4NGtPcTk2ZVpBSGlFYlc0STlwSnVlRS1nWFppaVpnRG9CVTNETTVITWN1U2V3Y0JvV1ZyNGc2ZjJvQzN6d3A4bmRZTllSWWduWUw5LVNfRlVaN0d1c2VKT2VRMkIzd3U5WU9GOThBQlVkWlFLLU9jZXdWQkkwRGJYbkJKYlpJNmctODZVWjVqc1l0VWJaVWZqSGdnMTkzZWhlS2pldVM2QnVVQWxOeXZZakRNeTE2b1djWUF0WlJ3aUdEaCIsImlzc3VlZF9hdCI6MTQ4OTcyNDM5MCwidXNlcl9pZCI6IjE3OTE0MzA3MjQ0NTk3ODIifQ'
}


# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'vaccine.middlewares.vaccineSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'vaccine.middlewares.vaccineDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'vaccine.pipelines.vaccinePipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import logging
from twisted.enterprise import adbapi
from scrapy.http import Request
from scrapy.exceptions import DropItem
import time
import pymysql.cursors
import socket
import select
import sys
import os
import errno


class vaccinePipeline(object):
    def __init__(self):  
        # self.dbpool = adbapi.ConnectionPool('pymysql', db='crecra',  
        #         user='root', passwd='qiuyongjie', cursorclass=pymysql.cursors.DictCursor,  
        #         charset='utf8', use_unicode=True)
        self.dbpool = adbapi.ConnectionPool('pymysql', host='127.0.0.1',db='vaccine',  
                user='root', passwd='qiuyongjie', cursorclass=pymysql.cursors.DictCursor,  
                charset='utf8', use_unicode=True)  
  
    def process_item(self, item, spider):  
        # run db query in thread pool 
        # Comment this to disable db insert. just output for testing
        # query = self.dbpool.runInteraction(self._conditional_insert, item)  
        # query.addErrback(self.handle_error)  
  
        return item  
  
    def _conditional_insert(self, tx, item):  
        # create record if doesn't exist.  
        # all this block run on it's own thread  
        tx.execute("select * from list where batch_no = %s", (item['batch_no']))  
        result = tx.fetchone()  
        if result:  
            logging.log(logging.WARNING,"!!!!!!!!!!  Item already exist: %s" % item['batch_no'])  
        else:  
            tx.execute(\
                "insert into list (name, spec, batch_no, amount, expired_at, vendor, signed_at, url, extra1, extra2, extra3, extra4) "  
                "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",  
                (
                 item['name'],  
                 item['spec'][0],
                 item['batch_no'],
                 item['amount'][0],
                 item['expired_at'][0],
                 item['vendor'][0],
                 item['signed_at'][0],
                 item['url'],
                 item['extra1'][0],
                 item['extra2'][0],
                 item['extra3'][0],
                 item['extra4'][0]
                 )
            ) 
            logging.log(logging.WARNING,">>>>>>>>>>  Item stored: %s" % item['name'])  
  
    def handle_error(self, e):  
        logging.log(logging.ERROR,e)

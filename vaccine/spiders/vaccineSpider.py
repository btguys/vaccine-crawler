# -*- coding: utf-8 -*-

import scrapy 
from vaccine.items import VaccineItem 
import time
import logging

class VaccineSpider(scrapy.Spider): 
    name = 'vaccine'

    #2007 http://www.nicpbp.org.cn/CL0168/ ok.   92612
    #2008 http://www.nicpbp.org.cn/CL0233/ ok.   90229
    #2009 http://www.nicpbp.org.cn/CL0427/ ok.   87301
    #2010 http://www.nicpbp.org.cn/CL0498/ ok.   83782
    #2011 http://www.nicpbp.org.cn/CL0568/ ok.   79419
    #2012 http://www.nicpbp.org.cn/CL0621/ ok    75216
    #2013 http://www.nicpbp.org.cn/CL0428/ ok    69898
    #2014 http://www.nicpbp.org.cn/CL0737/ ok.   65072
    #2015 http://www.nicpbp.org.cn/CL0792/ ok.   61102
    #2016 http://www.nicpbp.org.cn/CL0833/ ok.   57338 + 2xls
    #2017 http://www.nicpbp.org.cn/CL0873/ ok. 
    #2018 http://www.nicpbp.org.cn/CL0108/

    start_urls = ['http://www.nicpbp.org.cn/CL0737']

    # def start_requests(self, response):
    #     for url in response.xpath('//*[@id="table5"]/tbody/tr/td/table/tbody/tr/td[2]/font/a/@href'):
    #         logging.log(logging.WARNING,"---- current url: %s ----"%url)
    #         full_month_url = response.urljoin(url.extract())
    #         yield scrapy.Request(full_month_url,callback=self.parse)
    #         # time.sleep(100)
    #         logging.log(logging.WARNING,"---- current page done ----")

    
    def parse(self, response):

        # for 2007-2011
        # for url in response.xpath('//*[@id="table5"]/tbody/tr/td/table/tbody/tr/td[2]/font/a/@href'):

        # for 2012
        # for url in response.xpath('//*[@id="table5"]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr/td[2]/font/a/@href'):

        # for 2013-2018
        for url in response.xpath('//td[@class="ListColumnClass1"]/a/@href'):
            full_url = 'http://www.nicpbp.org.cn'+url.extract().replace("..", "")
            self.log("--------------------- current page %s ---------------------"%full_url)
            yield scrapy.Request(full_url,callback=self.parse_year_detail)

        # 暂停5秒，防止被服务器ban
        time.sleep(5)

        

    def parse_year_detail(self,response):
        # for common
        recordset = response.selector.xpath('/html/body/table/tr[position()>3]')

        # for 2013
        # for record in response.xpath('//table[@style="width:980px"]/tbody/tr'):

        # for 2014 2015

        if len(response.xpath('//meta[@name="ContentStart"]/following-sibling::div/table/tr')) >= 1:
            recordset = response.xpath('//meta[@name="ContentStart"]/following-sibling::div/table/tr')
        elif  len(response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tr')) >= 1:
            recordset = response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tr')
        elif  len(response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tbody/tr')) >= 1:
            recordset = response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tbody/tr')
        elif  len(response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tbody/tr/td/table/tbody/tr')) >= 1:
            recordset = response.xpath('//meta[@name="ContentStart"]/following-sibling::table/tbody/tr/td/table/tbody/tr')


        for record in recordset:


            item = VaccineItem()
            item['name'] = record.xpath('./td[2]').xpath('string(.)').extract()
            item['spec'] = record.xpath('./td[3]').xpath('string(.)').extract()
            item['batch_no'] = record.xpath('./td[4]').xpath('string(.)').extract()
            item['amount'] = record.xpath('./td[5]').xpath('string(.)').extract()
            item['expired_at'] = record.xpath('./td[6]').xpath('string(.)').extract()
            item['vendor'] = record.xpath('./td[7]').xpath('string(.)').extract()
            item['signed_at'] = record.xpath('./td[8]').xpath('string(.)').extract()
            item['extra1'] = (record.xpath('./td[9]').xpath('string(.)').extract() if len(record.xpath('./td'))>=9 else ['0'])
            item['extra2'] = (record.xpath('./td[10]').xpath('string(.)').extract() if len(record.xpath('./td'))>=10 else ['0'])
            item['extra3'] = (record.xpath('./td[11]').xpath('string(.)').extract() if len(record.xpath('./td'))>=11 else ['0'])
            item['extra4'] = (record.xpath('./td[12]').xpath('string(.)').extract() if len(record.xpath('./td'))>=12 else ['0'])
            item['url'] = response.url

            yield item
            
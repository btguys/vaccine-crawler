# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class VaccineItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
	name       = scrapy.Field()
	spec       = scrapy.Field()
	batch_no   = scrapy.Field()
	amount     = scrapy.Field()
	expired_at = scrapy.Field()
	vendor     = scrapy.Field()
	signed_at  = scrapy.Field()
	url        = scrapy.Field()
	extra1     = scrapy.Field()
	extra2     = scrapy.Field()
	extra3     = scrapy.Field()
	extra4     = scrapy.Field()
